Mos_Freq_Detection

This app is developed to demostrate the capability of using a novel nanomaterial based sensor to detect different targeted frequency.

freq_detect_version1_offline folder contains an offline version which simulate the real-time process by controlling the data acquisition speed through the local file. It is developed by using Python. The source code is in the freq_analysis folder.

freq_detect_version2_online folder contains an online version which process sensory data in real-time through aan AD7606 ADC board and a 5V DC power supply. It is developed by using C#. The source code is in within the folder. 

Other individual files and folders contain data and sound track for testing purpose.