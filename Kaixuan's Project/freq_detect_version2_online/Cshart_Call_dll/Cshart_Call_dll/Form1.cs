﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DLL;
using AD7606;
using System.Reflection;
using System.IO;
using System.Media;

namespace Cshart_Call_dll
{
   struct SAMPLE_DATA
   {
       ushort[] byDataArray;
    };
    public partial class Form1 : Form
    {
        public  CallBack  callback;//第二步，实例化委托类型，即定义一个delegate对象;
        bool USB_state = false;
        bool bHexDisplay = false;
        byte USB_DeviceNo = 0xff;
        Int32 cycles;
        List<SAMPLE_DATA> mList;
        Timer timer1;
        Bitmap im_no;
        Bitmap im_200;
        Bitmap im_450;
        Bitmap im_500;
        //Bitmap im_1400;
        Bitmap im_1200;
        ////2.声明事件；   
        //public event callback Alarm;

        ADC_CONFIG ADCOptions = new ADC_CONFIG();
         public Form1()
        {
            InitializeComponent();
        }
#region C# 动态库调用
        private void button1_Click(object sender, EventArgs e)
        {
            int result0;
            TestResult  result = new TestResult();
            result0 =result.Add (3,4);
            MessageBox.Show(result0 .ToString ());
        }
#endregion 

        void UpdateControl()
        {
            textBox9.Text = Convert.ToString(ADCOptions.wTrigSize);
            textBox12.Text = Convert.ToString(ADCOptions.wPeriod);
            textBox10.Text = Convert.ToString(ADCOptions.dwMaxCycles);
            textBox11.Text = Convert.ToString(ADCOptions.dwABDelay);
            if ((ADCOptions.byADCOptions & 0x20) == 0x20) 
                comboBox1.SelectedIndex = 1;
            else
                comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = (ADCOptions.byTrigOptions & 0x0C) >>2;
            comboBox3.SelectedIndex = ADCOptions.byTrigOptions & 0x03;
            comboBox4.SelectedIndex = (ADCOptions.byADCOptions & 0x10) >> 4;
            comboBox5.SelectedIndex = ADCOptions.byADCOptions & 0x07;
            comboBox6.SelectedIndex = (ADCOptions.byADCOptions & 0x08) >> 3;
        }

        public  void Form1_Load(object sender, EventArgs e)
        {
            GlobalVar.mHandle = this.Handle;
            DLLImport.M3F20xm_SetUSBNotify(true, GlobalVar.USB_Event);
            //采样 配置缺省值
            comboBox5.SelectedIndex = 7;//OS配置
            comboBox6.SelectedIndex = 0;//基准选择 外部/内部
            comboBox4.SelectedIndex = 0;//电压范围选择 ±5V，±10V
            comboBox1.SelectedIndex = 0;//时间单位uS/mA
//触发配置
            comboBox3.SelectedIndex = 1;//触发模式
            comboBox2.SelectedIndex = 0;//触发事件


           ADCOptions.byADCOptions =(byte) (comboBox5.SelectedIndex | (comboBox6.SelectedIndex << 3) | (comboBox4 .SelectedIndex<< 4) | (comboBox1.SelectedIndex<<5));
           ADCOptions.byTrigOptions= (byte)(comboBox3.SelectedIndex | (comboBox2.SelectedIndex << 2));
           richTextBox2.Clear();
           richTextBox2.AppendText( DateTime.Now+" Frequency Detection Demo!"+"\n");
            timer1 = new Timer();
            timer1.Interval = 500;
            timer1.Enabled = false;
            timer1.Tick += new EventHandler(timer1_Tick);//添加事件
            mList = new List<SAMPLE_DATA>();
            mList.Clear();
            cycles = 0;
            //Init the images
            //Try
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            Stream stream_no = myAssembly.GetManifestResourceStream("Cshart_Call_dll.NO.JPG");
            Stream stream_200 = myAssembly.GetManifestResourceStream("Cshart_Call_dll.200.JPG");
            Stream stream_450 = myAssembly.GetManifestResourceStream("Cshart_Call_dll.450.JPG");
            Stream stream_500 = myAssembly.GetManifestResourceStream("Cshart_Call_dll.500.JPG");
            //Stream stream_1400 = myAssembly.GetManifestResourceStream("Cshart_Call_dll.1400.png");
            Stream stream_1200 = myAssembly.GetManifestResourceStream("Cshart_Call_dll.1200.JPG");
            im_no = new Bitmap(stream_no);
            im_200 = new Bitmap(stream_200);
            im_450 = new Bitmap(stream_450);
            im_500 = new Bitmap(stream_500);
            //im_1400 = new Bitmap(stream_1400);
            im_1200 = new Bitmap(stream_1200);
        }
//触发模式配置
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ADCOptions.byTrigOptions = (byte)(comboBox3.SelectedIndex | (comboBox2.SelectedIndex << 2) | 0x80);
        }
        //采样配置
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ADCOptions.byADCOptions = (byte)(comboBox5.SelectedIndex | (comboBox6.SelectedIndex << 3) | (comboBox4.SelectedIndex << 4) | (comboBox1.SelectedIndex << 5));
        }
        #region  连接
        private void button3_Click(object sender, EventArgs e)
        {
            //if (DLLImport.M3F20xm_SetUSBNotify(false, callback))
            //{
            //    toolStripStatusLabel1.Text = "状态：连接"; 
            //}
            if (USB_state)
            {
                DLLImport.M3F20xm_CloseDevice(USB_DeviceNo);
                button3.Text = "Connect";
                toolStripStatusLabel1.Text = "Status：disconnected!";
                USB_state = false;
                richTextBox2.AppendText(DateTime.Now + " Device is disconnected!" + "\n");
                //让文本框获取焦点  
                richTextBox2.Focus();
                //设置光标的位置到文本尾  
                richTextBox2.Select(richTextBox2.TextLength, 0);
                ////滚动到控件光标处  
                //richTextBox2.ScrollToCaret();
                //richTextBox2.AppendText(value);
                
            }
            else
            {
                USB_DeviceNo = DLLImport.M3F20xm_OpenDevice();
                if (USB_DeviceNo == 0xFF)
                {
                    toolStripStatusLabel1.Text = "Status：Connection Failure!";
                    USB_state = false;
                    button3.Text = "Connect";
                    richTextBox2.AppendText(DateTime.Now + " Connection Failure!" + "\n");
                    //让文本框获取焦点  
                    richTextBox2.Focus();
                    //设置光标的位置到文本尾  
                    richTextBox2.Select(richTextBox2.TextLength, 0);
                    ////滚动到控件光标处  
                    //richTextBox2.ScrollToCaret();
                    //richTextBox2.AppendText(value);

                }
                else
                {
                    toolStripStatusLabel1.Text = "Status：Connected!";
                    USB_state = true;
                    button3.Text = "Disconnect";
                    richTextBox2.AppendText(DateTime.Now + " Device is connected!" + "\n");
                    //让文本框获取焦点  
                    richTextBox2.Focus();
                    //设置光标的位置到文本尾  
                    richTextBox2.Select(richTextBox2.TextLength, 0);
                    ////滚动到控件光标处  
                    //richTextBox2.ScrollToCaret();
                    //richTextBox2.AppendText(value);
                    Byte result = 0;
                    if (DLLImport.M3F20xm_Verify(USB_DeviceNo, ref result))
                    {
                       // MessageBox.Show("USB数据采集卡验证通过!");
                        DLLImport.M3F20xm_ADCGetConfig(USB_DeviceNo, ref ADCOptions);
                        UpdateControl();
                    }
                    else
                    {
                        MessageBox.Show("USB Data Collect Device is not validated!");
                    }
                }

            }
        }
        #endregion

        #region 设置按钮
        private void button4_Click(object sender, EventArgs e)
        {
            ADC_CONFIG tmpcfg;
            tmpcfg = ADCOptions;
            tmpcfg.wTrigSize = Convert.ToInt16(textBox9.Text);
            tmpcfg.wPeriod   = Convert.ToInt16(textBox12.Text);
            tmpcfg.dwMaxCycles = Convert.ToInt32(textBox10.Text);
            tmpcfg.dwABDelay = Convert.ToInt32(textBox11.Text);
            
            tmpcfg.byADCOptions = (byte)(comboBox5.SelectedIndex | (comboBox6.SelectedIndex << 3) | (comboBox4.SelectedIndex << 4) | (comboBox1.SelectedIndex << 5));
            tmpcfg.byTrigOptions = (byte)(comboBox3.SelectedIndex | (comboBox2.SelectedIndex << 2));

            if (DLLImport.M3F20xm_ADCSetConfig(USB_DeviceNo, ref tmpcfg))
            {
                MessageBox.Show("Config Set Done!");
                ADCOptions = tmpcfg;
            }
            else
            {
                MessageBox.Show("Config Set Failed!");
            }
        }
        #endregion
        private void timer1_Tick(object sender, EventArgs e)
        {
          Helper helper = new Helper();
          UInt32 pdwRealSize = 0;//实际读取的数据长度  
          byte [] lpBuffer = new byte[320000];//用来保存读取内容的缓存区    
          if (DLLImport.M3F20xm_ReadFIFO(USB_DeviceNo, lpBuffer, 320000, ref pdwRealSize))
            {
             if(pdwRealSize > 0)
                {
                    // Add the processing methods here
                    List<double[]> results = helper.Convertor(lpBuffer, pdwRealSize, comboBox4.SelectedIndex == 1);
                    //double[] chan_1 = results[0];
                    //double[] chan_2 = results[1];
                    //double[] chan_3 = results[2];
                    //double[] chan_4 = results[3];
                    //double[] chan_5 = results[4];
                    //double[] chan_6 = results[5];
                    //double[] chan_7 = results[6];
                    double[] chan_8 = results[7];
                    
                    //Detect Freq
                    var freq = helper.Freq_detector(chan_8, 10000, 256, 128);
                    //Test Image
                    //if (freq > 78 && freq < 79)
                    //{
                    //    im.Image = im_200;
                    //}
                    //else 
                    //{
                    //    im.Image = im_500;
                    //}
                    //Correct Logic
                    if (freq > 190 && freq < 210)
                    {
                        im.Image = im_200;
                    }
                    else if (freq > 440 && freq < 470)
                    {
                        im.Image = im_450;
                    }
                    else if (freq > 490 && freq < 510)
                    {
                        im.Image = im_500;
                    }
                    else if (freq > 1150 && freq < 1255) 
                    {
                        im.Image = im_1200;
                    }
                    else
                    {
                        im.Image = im_no;
                    }
                    richTextBox2.AppendText(DateTime.Now + "_Data Collected" + pdwRealSize.ToString() + "bytes\n");
                    richTextBox2.AppendText(DateTime.Now + "_Buffer Size" + chan_8.Length.ToString() + "\n");
                    //richTextBox2.AppendText(DateTime.Now + "_Detected Frequency: " + freq.ToString() + "Hz\n");
                    richTextBox2.Focus();
                    //设置光标的位置到文本尾  
                    richTextBox2.Select(richTextBox2.TextLength, 0);
                }
            }
        }
        /*
        #region 采样 一次
        private void button8_Click(object sender, EventArgs e)
        {
            String s="";
            float realVol,MaxVol;
            Int16[] byReadData={0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000};
            if(comboBox4.SelectedIndex == 1)
                MaxVol = 10;
            else
                MaxVol = 5;
            if (DLLImport.M3F20xm_ADCRead(USB_DeviceNo ,byReadData))
            {
                
                if (bHexDisplay)
                {
                    for (int i = 0; i < 8; i++)
                    {
                        s = s + "0X" + byReadData[i].ToString("X4") + "    ";
                    }
                }
                else
                {
                    for (int i = 0; i < 8; i++)
                    {
                        String str = "";
                        if ((byReadData[i] & 0x8000) == 0x8000)
                        {
                            byReadData[i] = (Int16)~byReadData[i];
                            //realVol = -1 * MaxVol * (code + 1) / 32768;
                            realVol = -1 * MaxVol * (byReadData[i] + 1) / 32768;
                            //str.Format("%3.6f   ",realVol);
                            str = String.Format("{0:0.000000}", realVol);
                        }
                        else
                        {
                            //realVol = MaxVol * (Sample[i] + 1) / 32768;
                            realVol = MaxVol * (byReadData[i] + 1) / 32768;
                            str = String.Format("{0:0.000000}", realVol);

                        }
                        s = s + str + "  ";
                    }
                }
                richTextBox1.AppendText(cycles.ToString("D8") + "   " + s + "\n");
                richTextBox1.Focus();
                //设置光标的位置到文本尾  
                richTextBox1.Select(richTextBox1.TextLength, 0);
                cycles++;
            }
            else
            {
                MessageBox.Show("Failure in reading sampled data!");
            }
        }
        #endregion
        */
        #region 连续采样
        private void button7_Click(object sender, EventArgs e)
        {
            
            //UInt32 dwBuffSize;//请求读取 的数据长度
            //UInt32 pdwRealSize=128;//实际读取的数据长度
            //dwBuffSize =Convert .ToUInt32(textBox9.Text );
            //byte [] lpBuffer = new byte[dwBuffSize];//用来保存读取内容的缓存区            
            if((ADCOptions.byTrigOptions & 0x80) == 0x80)    //sampling...
            {
                if (DLLImport.M3F20xm_ADCStop(USB_DeviceNo))
                {
                    button7.Text = "Start Sample";
                    ADCOptions.byTrigOptions &= 0x7F;
                    timer1.Enabled = false;;
                }
                return;
            }
            DLLImport.M3F20xm_InitFIFO(USB_DeviceNo);
            if (DLLImport.M3F20xm_ADCStart(USB_DeviceNo))
            {
                button7.Text = "Stop Sample";
                ADCOptions.byTrigOptions |= 0x80;
                timer1.Enabled = true;
            }
            else
            {
                MessageBox.Show("Failure in initiate sampling!");
            }

           // if (DLLImport.M3F20xm_ReadFIFO(USB_DeviceNo,lpBuffer,dwBuffSize ,ref pdwRealSize ))
           // {

           // }
           // else
           // {
            //    MessageBox.Show("获取采样数据失败!");
           // }

        }
        #endregion
        protected override void WndProc(ref Message m)
        {

            if (m.Msg == GlobalVar.WM_USB_STATUS)
            {
                if ((byte)m.WParam == USB_DeviceNo && (byte)m.LParam != 0x80)
                {
                    USB_DeviceNo = 255;
                    richTextBox2.AppendText(DateTime.Now + " Device is disconnected!" + "\n");
                    button3.Text = "Connect";
                    toolStripStatusLabel1.Text = "Status: Disconnected!";
                    USB_state = false;
                }
            }

            base.WndProc(ref m);
        }
        /*
        private void button2_Click(object sender, EventArgs e)
        {
            bHexDisplay = !bHexDisplay;
            if (bHexDisplay)
            {
                button2.Text = "Actual Value";
            }
            else
            {
                button2.Text = "Sample Value";
            }
        }
        */

        private void button6_Click(object sender, EventArgs e)
        {
            im.Image = im_no;
            richTextBox2.Clear();
            cycles = 0;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            //this.button9.Enabled = false;
            //this.button9.ForeColor = Color.Yellow;
            Assembly myAssembly_1 = Assembly.GetExecutingAssembly();
            Stream audio_200 = myAssembly_1.GetManifestResourceStream("Cshart_Call_dll.200.wav");      
            SoundPlayer splayer = new SoundPlayer(audio_200);
            splayer.Play();
            //this.button9.Enabled = true;
            //this.button9.BackColor = SystemColors.ControlLight;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Assembly myAssembly_1 = Assembly.GetExecutingAssembly();
            Stream audio_450 = myAssembly_1.GetManifestResourceStream("Cshart_Call_dll.450.wav");
            SoundPlayer splayer = new SoundPlayer(audio_450);
            splayer.Play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //this.button11.BackColor = Color.Green;
            Assembly myAssembly_1 = Assembly.GetExecutingAssembly();
            Stream audio_500 = myAssembly_1.GetManifestResourceStream("Cshart_Call_dll.500.wav");
            SoundPlayer splayer = new SoundPlayer(audio_500);
            splayer.Play();
            //this.button11.BackColor = Color.Gray;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //this.button12.BackColor = Color.Green;
            Assembly myAssembly_1 = Assembly.GetExecutingAssembly();
            Stream audio_1200 = myAssembly_1.GetManifestResourceStream("Cshart_Call_dll.1200.wav");
            SoundPlayer splayer = new SoundPlayer(audio_1200);
            splayer.Play();
            //this.button12.BackColor = Color.Gray;
        }

        private void button13_Click_1(object sender, EventArgs e)
        {
            Assembly myAssembly_1 = Assembly.GetExecutingAssembly();
            Stream audio_800 = myAssembly_1.GetManifestResourceStream("Cshart_Call_dll.800.wav");
            SoundPlayer splayer = new SoundPlayer(audio_800);
            splayer.Play();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            Assembly myAssembly_1 = Assembly.GetExecutingAssembly();
            Stream audio_320 = myAssembly_1.GetManifestResourceStream("Cshart_Call_dll.320.wav");
            SoundPlayer splayer = new SoundPlayer(audio_320);
            splayer.Play();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Assembly myAssembly_1 = Assembly.GetExecutingAssembly();
            Stream audio_1500 = myAssembly_1.GetManifestResourceStream("Cshart_Call_dll.1500.wav");
            SoundPlayer splayer = new SoundPlayer(audio_1500);
            splayer.Play();
        }

        ///*  ======================USB 插拔检测===========================    */
        //protected override void WndProc(ref Message m)
        //{
        //    //Console.WriteLine(m.WParam.ToInt32());    //打印程序检测到的变化信息
        //    try
        //    {
        //        //检测到USB口发生了变化,这里USB口变化时wParam的值是7，表示系统中添加或者删除了设备
        //        if (m.WParam.ToInt32() == 7)
        //        {
        //            UsBMethod(0);       //检测到USB口有变化时重新连接一次自己要检测的设备，连接不成功则可以判断设备已断开（个函数是USB连接函数）

        //            if (flag == 0)  　　//没找到设备处理事件，我这里 flag=0 表示设备没连接成功
        //            {
        //                Dispost();     //关闭设备，该函数的作用是关闭USB设备的连接
        //                MessageBox.Show(" USB 已断开！");
        //            }
        //            else
        //            {
        //                //这里可以添加设备没有断开的处理代码
        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);　　//异常处理函数
        //    }
        //    base.WndProc(ref m);　　//这个是windos的异常处理，一定要添加，不然会运行不了
        //}
        //----------------------------------------------------------------------
    }
}
