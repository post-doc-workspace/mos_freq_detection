﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

public delegate bool CallBack(byte bIndex, UInt32 dwUSBStatus);//定义一个委托CallBack(,)

public  struct ADC_CONFIG
{
    /*
      byADCOptions:

      bit5     for AD706 period unit            0- US, 1- MS
      bit4     for AD7606 range:                1- in +10V ~ -10V,    0- in +5v ~ -5v
      bit3     for AD7606 Ref selection:        1- Internal Ref,      0- External
      bit0~2   for AD7606 OS;
    */
   public Byte byADCOptions;
    /*
      byTrigOptions:

      bit7     for AD7606 samping:                1- in sampling,    0- stop
      bit2~3   for AD7606 IO selection:           00- falling, 01- Raising , 10- raising and falling
      bit0~1   for AD7606 trig mode;              00- GPIO trig, 01- period trig, 10- GPIO + period
    */

   public Byte byTrigOptions;

   public Int16 wTrigSize;//采样缓存大小，每到这个设定值后上传采样数据 
   public Int32 dwABDelay;//未使用，设置为0
    /* wPeriod: Timer trig period, unit byADCOptions bit 5~6 */
   public Int16 wPeriod;//周期采样 设置值
   public Int16 wReserved;//未使用
    /* dwTrigCnt: current trig counter */
   public Int32 dwCycleCnt;//当前 采样 轮数
    /* dwMaxCnt: Max Enabled trig number , trig will exit if dwTrigCnt is equal to dwMaxCnt */
   public Int32 dwMaxCycles;//当前采样轮数，如果为0则无穷大
}

namespace AD7606
{

    class DLLImport
    {


        /*--------------------------------DLL function import---------------------------------*/


        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_SetUSBNotify(bool bLoged, CallBack pUSB_CallBack);       

        [DllImport("ad7606.dll")]
        public static extern byte M3F20xm_OpenDevice();
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_Verify(byte byIndex,ref byte pResult);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_CloseDevice(byte bIndex);
        [DllImport("ad7606.dll")]
        public static extern byte M3F20xm_OpenDeviceByNumber(StringBuilder pSerial);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_CloseDeviceByNumber(StringBuilder pSerial);
        [DllImport("ad7606.dll")]
        public static extern byte M3F20xm_GetSerialNo(byte bIndex, StringBuilder pSerial);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_GetVersion(byte bIndex, byte bType, StringBuilder pVersion);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_DFUMode(byte byIndex);

       

        /*here are ADC functions*/
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_ADCGetConfig(byte bIndex, ref ADC_CONFIG pCfg);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_ADCSetConfig(byte bIndex, ref ADC_CONFIG pCfg);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_ADCRead(byte bIndex, Int16[] byReadData);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_ADCStart(byte bIndex);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_ADCStop(byte bIndex);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_ADCReset(byte bIndex);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_ADCStandBy(byte bIndex);  
           
        /*here are the FIFO function*/
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_InitFIFO(byte byIndex);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_ReadFIFO(byte byIndex, byte[] lpBuffer, UInt32 dwBuffSize, ref UInt32 pdwRealSize);
        [DllImport("ad7606.dll")]
        public static extern bool M3F20xm_GetFIFOLeft(byte byIndex, ref UInt32 pdwBuffsize);
        /*--------------------------------End---------------------------------*/
    }
}
