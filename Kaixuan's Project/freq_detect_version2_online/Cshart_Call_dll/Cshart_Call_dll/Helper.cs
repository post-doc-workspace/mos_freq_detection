﻿using MathNet.Filtering;
using MathNet.Filtering.FIR;
using Spectrogram;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cshart_Call_dll
{
    public class Helper
    {
        double[] freq_dict = new double[] { 0.0, 39.0625, 78.125, 117.1875, 156.25, 195.3125, 234.375, 273.4375, 312.5,
                351.5625, 390.625, 429.6875, 468.75, 507.8125, 546.875, 585.9375, 625.0, 664.0625, 703.125, 742.1875, 781.25,
                820.3125, 859.375, 898.4375, 937.5, 976.5625, 1015.625, 1054.6875, 1093.75, 1132.8125, 1171.875, 1210.9375, 1250.0,
                1289.0625, 1328.125, 1367.1875, 1406.25, 1445.3125, 1484.375, 1523.4375, 1562.5, 1601.5625, 1640.625, 1679.6875, 1718.75,
                1757.8125, 1796.875, 1835.9375, 1875.0, 1914.0625, 1953.125, 1992.1875, 2031.25, 2070.3125, 2109.375, 2148.4375, 2187.5, 2226.5625,
                2265.625, 2304.6875, 2343.75, 2382.8125, 2421.875, 2460.9375, 2500.0, 2539.0625, 2578.125, 2617.1875, 2656.25, 2695.3125, 2734.375,
                2773.4375, 2812.5, 2851.5625, 2890.625, 2929.6875, 2968.75, 3007.8125, 3046.875, 3085.9375, 3125.0, 3164.0625, 3203.125, 3242.1875,
                3281.25, 3320.3125, 3359.375, 3398.4375, 3437.5, 3476.5625, 3515.625, 3554.6875, 3593.75, 3632.8125, 3671.875, 3710.9375, 3750.0, 3789.0625,
                3828.125, 3867.1875, 3906.25, 3945.3125, 3984.375, 4023.4375, 4062.5, 4101.5625, 4140.625, 4179.6875, 4218.75, 4257.8125, 4296.875, 4335.9375,
                4375.0, 4414.0625, 4453.125, 4492.1875, 4531.25, 4570.3125, 4609.375, 4648.4375, 4687.5, 4726.5625, 4765.625, 4804.6875, 4843.75, 4882.8125,
                4921.875, 4960.9375};
        //FilterButterworth low_filter = new FilterButterworth(2000.0f, 10000, FilterButterworth.PassType.Lowpass, (float)(Math.Sqrt(2.0)));
        //FilterButterworth high_filter = new FilterButterworth(100.0f, 10000, FilterButterworth.PassType.Highpass, (float)(Math.Sqrt(2.0)));
        private static double fs = 10000; //Sampling Rate
        private static double fc1 = 100; //low cutoff frequency
        private static double fc2 = 2000; //high cutoff frequency
        //var bandpass = OnlineFirFilter.CreateBandpass(ImpulseResponse.Finite, fs, fc1, fc2);
        public List<double[]> Convertor(byte[] lpBuffer, UInt32 pdwRealSize, bool isTen)
        {
            List<double[]> channels = new List<double[]>();
            UInt32 datalength = (UInt32)(pdwRealSize + 1) / 16;
            double[] channel_1 = new double[datalength];
            double[] channel_2 = new double[datalength];
            double[] channel_3 = new double[datalength];
            double[] channel_4 = new double[datalength];
            double[] channel_5 = new double[datalength];
            double[] channel_6 = new double[datalength];
            double[] channel_7 = new double[datalength];
            double[] channel_8 = new double[datalength];
            int j = 0;
            for (int i = 0; i < pdwRealSize; i = i + 16)
            {
                double c_1 = toActualValue((short)(lpBuffer[i + 1] << 8 | lpBuffer[i]), isTen);
                channel_1[j] = c_1;
                double c_2 = toActualValue((short)(lpBuffer[i + 3] << 8 | lpBuffer[i + 2]), isTen);
                channel_2[j] = c_2;
                double c_3 = toActualValue((short)(lpBuffer[i + 5] << 8 | lpBuffer[i + 4]), isTen);
                channel_3[j] = c_3;
                double c_4 = toActualValue((short)(lpBuffer[i + 7] << 8 | lpBuffer[i + 6]), isTen);
                channel_4[j] = c_4;
                double c_5 = toActualValue((short)(lpBuffer[i + 9] << 8 | lpBuffer[i + 8]), isTen);
                channel_5[j] = c_5;
                double c_6 = toActualValue((short)(lpBuffer[i + 11] << 8 | lpBuffer[i + 10]), isTen);
                channel_6[j] = c_6;
                double c_7 = toActualValue((short)(lpBuffer[i + 13] << 8 | lpBuffer[i + 12]), isTen);
                channel_7[j] = c_7;
                double c_8 = toActualValue((short)(lpBuffer[i + 15] << 8 | lpBuffer[i + 14]), isTen);
                channel_8[j] = c_8;
                j++;
            }
            channels.Add(channel_1);
            channels.Add(channel_2);
            channels.Add(channel_3);
            channels.Add(channel_4);
            channels.Add(channel_5);
            channels.Add(channel_6);
            channels.Add(channel_7);
            channels.Add(channel_8);
            return channels;
        }
        private double toActualValue(short channel_in, bool isTen) 
        {
            double realVol, MaxVol;
            if (isTen)
                MaxVol = 10;
            else 
                MaxVol = 5;

            if ((channel_in & 0x8000) == 0x8000)
            {
                channel_in = (Int16)~channel_in;
                //realVol = -1 * MaxVol * (code + 1) / 32768;
                realVol = -1 * MaxVol * (channel_in + 1) / 32768;
                //str.Format("%3.6f   ",realVol);
                //str = String.Format("{0:0.000000}", realVol);
            }
            else
            {
                //realVol = MaxVol * (Sample[i] + 1) / 32768;
                realVol = MaxVol * (channel_in + 1) / 32768;
                //str = String.Format("{0:0.000000}", realVol);
            }
            return realVol;
        }

        public double Freq_detector(double[] signal, int SampleRate, int fftSize, int stepSize) 
        {
            //var bandpass = OnlineFirFilter.CreateBandpass(ImpulseResponse.Finite, fs, fc1, fc2);
            //double[] signal_p = bandpass.ProcessSamples(signal);
            List<int> index_collection = new List<int>();
            var sg = new SpectrogramGenerator(SampleRate, fftSize, stepSize);
            sg.Add(signal);
            List<double[]> v2 = sg.GetFFTs();
            for (int q = 0; q < v2.Count; q++)
            {
                // Remove High-Low Frequency Noise
                for (int k = 0; k < freq_dict.Length; k++)
                {
                    if (k == 0)
                    {
                        v2[q][k] = 0;
                    }
                    else if (k == 1)
                    {
                        v2[q][k] = 0;
                    }
                    else if (k == 2)
                    {
                        v2[q][k] = 0;
                    }
                    else if (k > 52)  //52 
                    {
                        v2[q][k] = 0;
                    }
                    else
                    {
                        continue;
                    }
                }
                //v2[q][0] = 0;
                //v2[q][1] = 0;
                //v2[q][2] = 0;
                double maxV = v2[q].Max();
                int maxIndex = v2[q].ToList().IndexOf(maxV);
                index_collection.Add(v2[q].ToList().IndexOf(v2[q].Max()));
            }
            var most_index = index_collection.GroupBy(i => i).OrderByDescending(grp => grp.Count())
  .Select(grp => grp.Key).First();
            //if (most_index > freq_dict.Length) 
            //{
            //    most_index = 0;
            //}
            double detected_freq = freq_dict[most_index];
            index_collection.Clear();
            return detected_freq;
        }
    }
}
