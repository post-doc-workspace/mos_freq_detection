﻿Module userdefine
    Public WM_USB_STATUS = &H400 + 100
    Public WM_TRIG_STATUS = &H400 + 101
    Public mHandle As Integer
    Public sFormTitle As String
    Public Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hwnd As Int32, ByVal wMsg As Int32, ByVal wParam As Int32, ByVal lParam As Int32) As Long
    Private Declare Auto Function FindWindow Lib "user32" (ByVal lpClassName As String, ByVal lpWindowName As String) As Integer
    Public Delegate Function CallBack(ByVal byIndex As Byte, ByVal dwStatus As UInteger) As Boolean

    Public Function USB_Event(ByVal byIndex As Byte, ByVal dwUSBStatus As UInteger) As Boolean
        mHandle = FindWindow(vbNullString, sFormTitle)
        PostMessage(mHandle, WM_USB_STATUS, byIndex, dwUSBStatus)
        Return True
    End Function
    Public Function Trig_Event(ByVal byIndex As Byte, ByVal dwTrigStatus As UInteger) As Boolean
        mHandle = FindWindow(vbNullString, sFormTitle)
        PostMessage(mHandle, WM_TRIG_STATUS, byIndex, dwTrigStatus)
        Return True
    End Function
End Module

Public Structure ADCCONFIG
Dim byADCOptions As Byte
Dim byTrigOptions as Byte
Dim wTrigSize As short
Dim dwABDelay As UInteger
Dim wPeriod As UInteger
Dim wReserved As UInteger
Dim dwCycleCnt As UInteger
Dim dwMaxCycles As UInteger
End Structure

Module ExtenalDll



    Public Declare Function M3F20xm_SetUSBNotify Lib "ad7606.dll" Alias "_M3F20xm_SetUSBNotify@8" (ByVal bLoged As Boolean, ByVal pUSB_CallBack As CallBack) As Boolean
  
    Public Declare Function M3F20xm_OpenDevice Lib "ad7606.dll" Alias "_M3F20xm_OpenDevice@0" () As Byte
    Public Declare Function M3F20xm_OpenDeviceByNumber Lib "ad7606.dll" Alias "_M3F20xm_OpenDeviceByNumber@4" (ByVal pSerialString As String) As Byte
    Public Declare Function M3F20xm_CloseDevice Lib "ad7606.dll" Alias "_M3F20xm_CloseDevice@4" (ByVal byIndex As Byte) As Boolean
    Public Declare Function M3F20xm_CloseDeviceByNumber Lib "ad7606.dll" Alias "_M3F20xm_CloseDeviceByNumber@4" (ByVal pSerialString As String) As Boolean
    Public Declare Function M3F20xm_GetVersion Lib "ad7606.dll" Alias "_M3F20xm_GetVersion@12" (ByVal byIndex As Byte, ByVal byType As Byte, ByVal lpBuffer As String) As Boolean
    Public Declare Function M3F20xm_GetSerialNo Lib "ad7606.dll" Alias "_M3F20xm_GetSerialNo@8" (ByVal byIndex As Byte, ByVal lpBuff As String) As Byte
    Public Declare Function M3F20xm_Verify Lib "ad7606.dll" Alias "_M3F20xm_Verify@8" (ByVal byIndex As Byte,ByRef lpresult As Byte) As Boolean
   
    Public Declare Function M3F20xm_InitFIFO Lib "ad7606.dll" Alias "_M3F20xm_InitFIFO@4" (ByVal byIndex As Byte) As Boolean
    Public Declare Function M3F20xm_ReadFIFO Lib "ad7606.dll" Alias "_M3F20xm_ReadFIFO@16" (ByVal byIndex As Byte, ByVal lpReadBuffer(),ByVal dwBuffSize As UInteger,ByRef pdwRealSize As UInteger) As Boolean
    Public Declare Function M3F20xm_GetFIFOLeft Lib "ad7606.dll" Alias "_M3F20xm_GetFIFOLeft@8" (ByVal byIndex As Byte, ByRef pdwBuffsize As UInteger) As Boolean

    Public Declare Function M3F20xm_ADCGetConfig Lib "ad7606.dll" Alias "_M3F20xm_ADCGetConfig@8" (ByVal byIndex As Byte, ByRef pcfg As ADCCONFIG) As Boolean
    Public Declare Function M3F20xm_ADCSetConfig Lib "ad7606.dll" Alias "_M3F20xm_ADCSetConfig@8" (ByVal byIndex As Byte, ByVal pcfg As ADCCONFIG) As Boolean
    Public Declare Function M3F20xm_ADCRead Lib "ad7606.dll" Alias "_M3F20xm_ADCRead@8" (ByVal byIndex As Byte, ByVal lpReadBuffer() As Short) As Boolean
    Public Declare Function M3F20xm_ADCStart Lib "ad7606.dll" Alias "_M3F20xm_ADCStart@4" (ByVal byIndex As Byte) As Boolean
    Public Declare Function M3F20xm_ADCStop Lib "ad7606.dll" Alias "_M3F20xm_ADCStop@4" (ByVal byIndex As Byte) As Boolean
    Public Declare Function M3F20xm_ADCReset Lib "ad7606.dll" Alias "_M3F20xm_ADCReset@4" (ByVal byIndex As Byte) As Boolean
    Public Declare Function M3F20xm_ADCStandBy Lib "ad7606.dll" Alias "_M3F20xm_ADCStandBy@4" (ByVal byIndex As Byte) As Boolean

  









End Module
