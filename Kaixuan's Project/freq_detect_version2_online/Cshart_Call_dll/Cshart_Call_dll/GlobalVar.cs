﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;


namespace Cshart_Call_dll
{
    class GlobalVar
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr PostMessage(IntPtr hWnd, int msg, int wParam, int lParam);



        public const int WM_USB_STATUS = 0x400 + 100;  
        public static IntPtr mHandle;


        public static bool USB_Event(byte bIndex, UInt32 dwUSBStatus)
        {
            PostMessage(mHandle, WM_USB_STATUS, (int)bIndex, (int)dwUSBStatus);
            return true;
        }    
    }
}
