﻿using Spectrogram;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            double[] freq_dict = new double[] { 0.0, 39.0625, 78.125, 117.1875, 156.25, 195.3125, 234.375, 273.4375, 312.5, 
                351.5625, 390.625, 429.6875, 468.75, 507.8125, 546.875, 585.9375, 625.0, 664.0625, 703.125, 742.1875, 781.25, 
                820.3125, 859.375, 898.4375, 937.5, 976.5625, 1015.625, 1054.6875, 1093.75, 1132.8125, 1171.875, 1210.9375, 1250.0, 
                1289.0625, 1328.125, 1367.1875, 1406.25, 1445.3125, 1484.375, 1523.4375, 1562.5, 1601.5625, 1640.625, 1679.6875, 1718.75, 
                1757.8125, 1796.875, 1835.9375, 1875.0, 1914.0625, 1953.125, 1992.1875, 2031.25, 2070.3125, 2109.375, 2148.4375, 2187.5, 2226.5625, 
                2265.625, 2304.6875, 2343.75, 2382.8125, 2421.875, 2460.9375, 2500.0, 2539.0625, 2578.125, 2617.1875, 2656.25, 2695.3125, 2734.375, 
                2773.4375, 2812.5, 2851.5625, 2890.625, 2929.6875, 2968.75, 3007.8125, 3046.875, 3085.9375, 3125.0, 3164.0625, 3203.125, 3242.1875, 
                3281.25, 3320.3125, 3359.375, 3398.4375, 3437.5, 3476.5625, 3515.625, 3554.6875, 3593.75, 3632.8125, 3671.875, 3710.9375, 3750.0, 3789.0625, 
                3828.125, 3867.1875, 3906.25, 3945.3125, 3984.375, 4023.4375, 4062.5, 4101.5625, 4140.625, 4179.6875, 4218.75, 4257.8125, 4296.875, 4335.9375, 
                4375.0, 4414.0625, 4453.125, 4492.1875, 4531.25, 4570.3125, 4609.375, 4648.4375, 4687.5, 4726.5625, 4765.625, 4804.6875, 4843.75, 4882.8125, 
                4921.875, 4960.9375};
            string[] lines = System.IO.File.ReadAllLines(@"E:\PostDoc_Workspace\c_sharp_freq_analysis\test_data\new_data\test.txt");
            // divide into the segments
            List<double[]> segments = new List<double[]>();
            
            int window_size = 512;
            float overlap_rate = 0.5f;
            //double[] temp = new double[lines.Length];
            List<int> index_collection = new List<int>();

            for (int i = 0; i < lines.Length; i = (int)(i + window_size * (1 - overlap_rate)))
            {
                if (i + window_size > lines.Length)
                    break;
                else
                {
                    double[] temp = new double[window_size];
                    for (int j = 0; j < window_size; j++)
                    {
                        temp[j] = Convert.ToDouble(lines[i + j]);
                    }
                    segments.Add(temp);
                }
            }
            //for(int i = 0; i<lines.Length; i++)
            //{
            //    temp[i] = Convert.ToDouble(lines[i]);
            //}
            //segments.Add(temp);

            Console.WriteLine("lenght:" + segments.Count + " \n\n");
            for (int k = 0; k < segments.Count; k++)
            {
                var sg = new SpectrogramGenerator(10000, 256, 128);
                sg.Add(segments[k]);
                List<double[]> v2 = sg.GetFFTs();
                for (int q = 0; q < v2.Count; q++)
                {
                    //Remove High-Low Frequency Noise
                    for (int j = 0; j < freq_dict.Length; j++)
                    {
                        if (j == 0)
                        {
                            v2[q][j] = 0;
                        }
                        else if (j == 1)
                        {
                            v2[q][j] = 0;
                        }
                        else if (j > 52)  //52
                        {
                            v2[q][j] = 0;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    // Remove High Low Frequency Noise
                    //v2[q][0] = 0;
                    //v2[q][1] = 0;
                    double maxV = v2[q].Max();
                    int maxIndex = v2[q].ToList().IndexOf(maxV);
                    index_collection.Add(v2[q].ToList().IndexOf(v2[q].Max()));
                }
                var most_index = index_collection.GroupBy(i => i).OrderByDescending(grp => grp.Count())
      .Select(grp => grp.Key).First();
                var detected_freq = freq_dict[most_index];
                Console.WriteLine("Frequency:" + detected_freq + "Hz \n");
                index_collection.Clear();
            }
        }
    }
}
